import {
  Modal,
  Box,
  TextField,
  Grid,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
  Snackbar,
  Alert
} from "@mui/material";
import { useState } from "react";
import ModalError from "../ModalError/ModalError";
import ModalSuccess from "../ModalSuccess/ModalSuccess";

function ModalAddNewUser(props) {
  // Fetch Api
  const fetchApi = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };
  // Style Component
  const styleBox = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 800,
    bgcolor: "background.paper",
    border: "1px solid #333",
    borderRadius: 2,
    boxShadow: 24,
    p: 4,
  };
  const styleTextField = {
    width: "95%",
  };
  // State
  const [pizzaCombo, setPizzaCombo] = useState("");
  const [pizzaType, setPizzaType] = useState("");
  const [drink, setDrink] = useState("");
  const [total, setTotal] = useState("");
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [address, setAddress] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [voucher, setVoucher] = useState("");

  // Modal Error
  const [showModalError, setShowModalError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const handleCloseModalError = () => setShowModalError(false);
  const handleShowModalError = () => setShowModalError(true);

  // Modal Succses
  const [showModalSuccess, setShowModalSuccess] = useState(false);
  const [orderId, setOrderId] = useState("");
  const handleCloseModalSuccess = () => setShowModalSuccess(false);
  const handleShowModalSuccess = () => setShowModalSuccess(true);

  // Toast Voucher Error
  const [showSnackbar, setShowSnackbar] = useState(false);
  const handleCloseSnackbar = () => setShowSnackbar(false);
  const handleShowSnackbar = () => setShowSnackbar(true);

  // Handle Event
  const handleClose = () => {
    props.setOpenModalAddNewUser(false);
  };

  //Lấy Dữ Liệu Combo
  const selectSizePizza = (event) => {
    const selectValue = event.target.value;
    if (selectValue === "S") {
      setPizzaCombo({
        kichCo: "S",
        duongKinh: "20",
        suon: "2",
        salad: "200",
        soLuongNuoc: "2",
        thanhTien: "150000",
      });
      setTotal(150000);
    }
    if (selectValue === "M") {
      setPizzaCombo({
        kichCo: "M",
        duongKinh: "25",
        suon: "4",
        salad: "300",
        soLuongNuoc: "3",
        thanhTien: "200000",
      });
      setTotal(200000);
    }
    if (selectValue === "L") {
      setPizzaCombo({
        kichCo: "L",
        duongKinh: "30",
        suon: "8",
        salad: "500",
        soLuongNuoc: "4",
        thanhTien: "250000",
      });
      setTotal(250000);
    }
  };

  //Lấy Dữ Liệu Type
  const selectTypePizza = (event) => {
    const selectValue = event.target.value;
    setPizzaType(selectValue);
  };

  //Lấy Dữ Liệu Drink
  const selectDrink = (event) => {
    const selectValue = event.target.value;
    setDrink(selectValue);
  };

  //Lấy Thông Tin User
  const onChangeUserName = (event) => {
    const inputValue = event.target.value;
    setName(inputValue);
  };
  const onChangeUserPhoneNumber = (event) => {
    const inputValue = event.target.value;
    setPhoneNumber(inputValue);
  };
  const onChangeUserAddress = (event) => {
    const inputValue = event.target.value;
    setAddress(inputValue);
  };
  const onChangeUserEmail = (event) => {
    const inputValue = event.target.value;
    setEmail(inputValue);
  };
  const onChangeUserMessage = (event) => {
    const inputValue = event.target.value;
    setMessage(inputValue);
  };
  const onChangeUserVoucher = (event) => {
    const inputValue = event.target.value;
    setVoucher(inputValue);
  };

  // Hàm Tạo Order
  const createOrder = () => {
    const validatedUserOrder = validateOrder();
    if (validatedUserOrder) {
      callApiCreateOrder();
    }
  };
  const validateOrder = () => {
    // Check User Info
    if (name.trim() === "") {
      handleShowModalError();
      setErrorMessage("Chưa Nhập Tên");
      return false;
    }
    if (phoneNumber.trim() === "") {
      handleShowModalError();
      setErrorMessage("Chưa Nhập Số Điện Thoại");
      return false;
    }
    if (isNaN(phoneNumber.trim())) {
      handleShowModalError();
      setErrorMessage("Số Điện Thoại Nhập Chưa Đúng");
      return false;
    }

    if (address.trim() === "") {
      handleShowModalError();
      setErrorMessage("Chưa Nhập Địa Chỉ");
      return false;
    }
    if (email.trim() === "") {
      handleShowModalError();
      setErrorMessage("Chưa Nhập Email");
      return false;
    }

    var vEmailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if (!vEmailRegex.test(email)) {
      handleShowModalError();
      setErrorMessage("Email Nhập Chưa Đúng Chuẩn");
      return false;
    }

    if (pizzaCombo === "") {
      handleShowModalError();
      setErrorMessage("Chưa Chọn Kích Cỡ Pizza");
      return false;
    }
    if (pizzaType === "") {
      handleShowModalError();
      setErrorMessage("Chưa Chọn Loại Pizza");
      return false;
    }
    if (drink === "") {
      handleShowModalError();
      setErrorMessage("Chưa Chọn Loại Nước Uống");
      return false;
    }

    if (voucher.trim() !== "") {
      checkVoucherById();
    }
    return true;
  };

  // Kiểm Tra Voucher
  const checkVoucherById = () => {
    callApiGetVoucher()
      .then((data) => {
        console.log(data);
        // Tính lại thành tiền khi voucher đúng
        setTotal(total - (total * data.phanTramGiamGia) / 100);
      })
      .catch((error) => {
        console.log(error);
        handleShowSnackbar();
        setVoucher("");
      });
  };
  // Call Api lấy voucher
  const callApiGetVoucher = async () => {
    const response = await fetch(
      `http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/${voucher}`
    );
    const data = await response.json();
    return data;
  };

  // Call Api Create Order
  const callApiCreateOrder = () => {
    let orderRequest = {
      kichCo: pizzaCombo.kichCo,
      duongKinh: pizzaCombo.duongKinh,
      suon: pizzaCombo.suon,
      salad: pizzaCombo.salad,
      loaiPizza: pizzaType,
      idVourcher: voucher,
      idLoaiNuocUong: drink,
      soLuongNuoc: pizzaCombo.soLuongNuoc,
      hoTen: name,
      thanhTien: total,
      email: email,
      soDienThoai: phoneNumber,
      diaChi: address,
      loiNhan: message,
    };
    const order = {
      method: "POST",
      body: JSON.stringify(orderRequest),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://42.115.221.44:8080/devcamp-pizza365/orders", order)
      .then((data) => {
        console.log(data);
        setOrderId(data.orderId);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        // Đóng modal addUser Hiện modal Succsess
        props.setOpenModalAddNewUser(false);
        setTimeout(() => {
          handleShowModalSuccess();
        }, 500);
      });
  };
  
  return (
    <>
      <Modal
        open={props.openModalAddNewUser}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={styleBox}>
          <Grid container>
            <Grid item xs={12} md={12} sm={12} lg={12}>
              <Typography
                sx={{ py: "0.5rem" }}
                variant="h4"
                component="h4"
                align="left"
                fontWeight="bold"
              >
                User Info
              </Typography>
            </Grid>
            <Grid container xs={12} md={12} sm={12} lg={12}>
              <Grid item xs={6} md={6} sm={6} lg={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Họ Tên"
                  size="medium"
                  sx={styleTextField}
                  value={name}
                  onChange={onChangeUserName}
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Số Điện Thoại"
                  size="medium"
                  sx={styleTextField}
                  value={phoneNumber}
                  onChange={onChangeUserPhoneNumber}
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  required
                  id="outlined-required"
                  label="Địa Chỉ"
                  size="medium"
                  sx={styleTextField}
                  value={address}
                  onChange={onChangeUserAddress}
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  required
                  id="outlined-required"
                  label="Email"
                  size="medium"
                  sx={styleTextField}
                  value={email}
                  onChange={onChangeUserEmail}
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  id="outlined-required"
                  label="Lời Nhắn"
                  size="medium"
                  sx={styleTextField}
                  value={message}
                  onChange={onChangeUserMessage}
                />
              </Grid>
            </Grid>
            {/* Pizza Size */}
            <Grid item xs={12} md={12} sm={12} lg={12}>
              <Typography
                sx={{ py: "0.5rem" }}
                variant="h4"
                component="h4"
                align="left"
                fontWeight="bold"
              >
                Pizza Order
              </Typography>
            </Grid>
            <Grid container xs={12} md={12} sm={12} lg={12}>
              <Grid item xs={6} md={6} sm={6} lg={6}>
                <FormControl fullWidth>
                  <InputLabel>Kích Cỡ</InputLabel>
                  <Select
                    onChange={selectSizePizza}
                    label="Kích Cỡ"
                    size="medium"
                    sx={styleTextField}
                    value={pizzaCombo !== "" ? pizzaCombo.kichCo : ""}
                  >
                    <MenuItem value="S">Size S</MenuItem>
                    <MenuItem value="M">Size M</MenuItem>
                    <MenuItem value="L">Size L</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6}>
                <TextField
                  label={pizzaCombo !== "" ? "" : "Đường Kính"}
                  value={
                    pizzaCombo !== ""
                      ? `Đường Kính: ${pizzaCombo.duongKinh}`
                      : null
                  }
                  required
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  label={pizzaCombo !== "" ? "" : "Sườn"}
                  value={pizzaCombo !== "" ? `Sườn: ${pizzaCombo.suon}` : null}
                  required
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  label={pizzaCombo !== "" ? "" : "Salad"}
                  value={
                    pizzaCombo !== "" ? `Salad: ${pizzaCombo.salad}` : null
                  }
                  required
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  label={pizzaCombo !== "" ? "" : "Số Lượng Nước"}
                  value={
                    pizzaCombo !== ""
                      ? `Số Lượng Nước: ${pizzaCombo.soLuongNuoc}`
                      : null
                  }
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                  required
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  label={pizzaCombo !== "" ? "" : "Thành Tiền"}
                  value={
                    pizzaCombo !== ""
                      ? `Thành Tiền: ${pizzaCombo.thanhTien}`
                      : null
                  }
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                  required
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <FormControl fullWidth>
                  <InputLabel>Loại Pizza</InputLabel>
                  <Select
                    label="Loại Pizza"
                    size="medium"
                    sx={styleTextField}
                    onChange={selectTypePizza}
                  >
                    <MenuItem value="Seafood">Hải sản</MenuItem>
                    <MenuItem value="Hawaii">Hawaii</MenuItem>
                    <MenuItem value="Bacon">Thịt hun khói</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <FormControl fullWidth>
                  <InputLabel>Loại Nước Uống</InputLabel>
                  <Select
                    label="Loại Nước Uống"
                    size="medium"
                    sx={styleTextField}
                    onChange={selectDrink}
                  >
                    {props.drinkList.map((drink, index) => {
                      return (
                        <MenuItem value={drink.maNuocUong}>
                          {drink.tenNuocUong}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} md={12} sm={12} lg={12} pt={2}>
                <TextField
                  id="outlined-required"
                  label="Mã Voucher"
                  size="medium"
                  style={{ width: "97%" }}
                  value={voucher}
                  onChange={onChangeUserVoucher}
                />
              </Grid>
              <Grid item xs={12} md={12} sm={12} lg={12} pt={2}>
                <Button
                  className="fw-bold"
                  variant="contained"
                  color="success"
                  size="small"
                  style={{ padding: "10px", float: "right" }}
                  // Gọi Hàm Xem Chi Tiết User Khi Click
                  // Input userObj
                  onClick={createOrder}
                >
                  Create Order
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      {/* Modal Error */}
      <ModalError
        showModalError={showModalError}
        handleCloseModalError={handleCloseModalError}
        errorMessage={errorMessage}
      ></ModalError>
      {/* Modal Success */}
      <ModalSuccess
        showModalSuccess={showModalSuccess}
        handleCloseModalSuccess={handleCloseModalSuccess}
        orderId={orderId}
      ></ModalSuccess>
      {/* Toast */}
      <Snackbar
        open={showSnackbar}
        autoHideDuration={3000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity="error"
          sx={{ width: "100%" }}
        >
          Mã Voucher Không Tồn Tại
        </Alert>
      </Snackbar>
    </>
  );
}

export default ModalAddNewUser;
