import { Modal, Box, Typography } from "@mui/material";
import { FaPizzaSlice } from 'react-icons/fa';

function ModalSuccess(props) {
  const styleModal = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 500,
    bgcolor: "background.paper",
    border: "1px solid #333",
    borderRadius: "4px",
    overflow: "hidden",
    boxShadow: 10,
    p: 0,
  };
  const styleHeader = {
    bgcolor: "success.main",
    p: 1,
    fontWeight: "bold",
    color: "#FFF",
  };
  const styleContent = {
    bgcolor: "background.paper",
    p: 2,
    m: 0,
    fontSize: "24px",
    fontStyle:"italic"
  };
  
  // Đóng modal
  const closeModalSuccess = () => {
    props.handleCloseModalSuccess();
    setTimeout(() => {reloadPage()},500);
  }

  const reloadPage = () => {
    window.location.reload()
  }
  return (
    <Modal
      open={props.showModalSuccess}
      onClose={closeModalSuccess}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={styleModal}>
        <Typography
          sx={styleHeader}
          id="modal-modal-title"
          variant="h6"
          component="h2"
        >
          <FaPizzaSlice className="fs-2"></FaPizzaSlice> Tạo Đơn Hàng Thành Công
        </Typography>
        <Typography sx={styleContent} id="modal-modal-description">
          Mã Đơn Hàng {props.orderId}
        </Typography>
      </Box>
    </Modal>
  );
}

export default ModalSuccess;
