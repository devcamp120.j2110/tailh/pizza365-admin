import { Modal, Box, Typography } from "@mui/material";
import { BiError } from 'react-icons/bi';

function ModalError(props) {
  //Style
  const styleModalError = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 500,
    bgcolor: "background.paper",
    border: "1px solid #333",
    borderRadius: "4px",
    overflow: "hidden",
    boxShadow: 10,
    p: 0,
  };
  const styleHeader = {
    bgcolor: "error.main",
    p: 1,
    fontWeight: "bold",
    color: "#FFF",
  };
  const styleContent = {
    bgcolor: "background.paper",
    p: 2,
    m: 0,
    fontSize: "24px",
    fontStyle:"italic",
    fontWeight: "bold",
  };
  return (
    <Modal
      open={props.showModalError}
      onClose={props.handleCloseModalError}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={styleModalError}>
        <Typography
          sx={styleHeader}
          id="modal-modal-title"
          variant="h6"
          component="h2"
        >
          <BiError className="fs-2"></BiError> Thông Tin Chưa Chính Xác
        </Typography>
        <Typography sx={styleContent} id="modal-modal-description">
          {props.errorMessage}
        </Typography>
      </Box>
    </Modal>
  );
}

export default ModalError;
