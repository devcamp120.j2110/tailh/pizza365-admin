import { Modal, Box, Typography, Button, Snackbar, Alert } from "@mui/material";
import { useState } from "react";
import { BiError } from "react-icons/bi";

function ModalDeleteUser(props) {
  // Style
  const styleModalError = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 500,
    bgcolor: "background.paper",
    border: "1px solid #333",
    borderRadius: "4px",
    overflow: "hidden",
    boxShadow: 10,
    p: 0,
  };
  const styleHeader = {
    bgcolor: "error.main",
    p: 1,
    fontWeight: "bold",
    color: "#FFF",
  };
  const styleContent = {
    bgcolor: "background.paper",
    p: 2,
    m: 0,
    fontSize: "24px",
    fontStyle: "italic",
    fontWeight: "bold",
  };
  //   Handle
  const handleCloseModalDelete = () => {
    props.setOpenModalDeleteUser(false);
  };
  // Toast
  const [showSnackbar, setShowSnackbar] = useState(false);
  const handleCloseSnackbar = () => setShowSnackbar(false);
  const handleShowSnackbar = () => setShowSnackbar(true);
  //Fetch Api
  const fetchApi = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };
  const deleteOrder = () => {
    const orderDelete = {
      method: "DELETE",
    };
    fetchApi(
      `http://42.115.221.44:8080/devcamp-pizza365/orders/${props.userOrder.id}`,
      orderDelete
    );
    handleCloseModalDelete();
    handleShowSnackbar();
    setTimeout(() => {
      window.location.reload();
    }, 1500);
  };
  return (
    <>
      <Modal
        open={props.openModalDeleteUser}
        onClose={handleCloseModalDelete}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={styleModalError}>
          <Typography
            sx={styleHeader}
            id="modal-modal-title"
            variant="h6"
            component="h2"
          >
            <BiError className="fs-2"></BiError> Xác Nhận Xoá Đơn Hàng
          </Typography>
          <Typography sx={styleContent} id="modal-modal-description">
            Mã Đơn Hàng: {props.userOrder.orderId}
          </Typography>

          <Button
            className="fw-bold"
            variant="contained"
            color="warning"
            size="medium"
            style={{
              padding: "10px",
              float: "right",
              marginBottom: "10px",
              marginRight: "10px",
            }}
            // Gọi Hàm Xem Chi Tiết User Khi Click
            // Input userObj
            onClick={deleteOrder}
          >
            Xác Nhận
          </Button>
        </Box>
      </Modal>
      {/* Toast */}
      <Snackbar
        open={showSnackbar}
        autoHideDuration={1000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity="success"
          sx={{ width: "100%" }}
        >
          Xoá Order Thành Công
        </Alert>
      </Snackbar>
    </>
  );
}

export default ModalDeleteUser;
