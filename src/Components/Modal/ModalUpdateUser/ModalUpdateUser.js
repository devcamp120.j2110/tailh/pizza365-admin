import {
  Modal,
  Box,
  TextField,
  Grid,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
  Snackbar,
  Alert,
} from "@mui/material";
import { useState } from "react";

function ModalUpdateUser(props) {
  const loaiPizza = props.userOrder.loaiPizza ? props.userOrder.loaiPizza.toLowerCase() : null;
  const idLoaiNuocUong = props.userOrder.idLoaiNuocUong ? props.userOrder.idLoaiNuocUong.toLowerCase() : null;
  // State
  const [orderStatus, setOrderStatus] = useState("");
  // Toast Voucher Error
  const [showSnackbar, setShowSnackbar] = useState(false);
  const handleCloseSnackbar = () => setShowSnackbar(false);
  const handleShowSnackbar = () => setShowSnackbar(true);

  //Fetch Api
  const fetchApi = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };

  // Style Component
  const styleBox = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 800,
    bgcolor: "background.paper",
    border: "1px solid #333",
    borderRadius: 2,
    boxShadow: 24,
    p: 4,
  };
  const styleTextField = {
    width: "95%",
  };
  // Handle
  const handleCloseModalUpdateUser = () => {
    props.setOpenModalUpdateUser(false);
  };

  const onChangeSelectStatus = (event) => {
    var valueSelect = event.target.value;
    setOrderStatus(valueSelect);
  };
  //Hàm update order
  const updateStatusOrder = () => {
    console.log("Update Order");
    let vObjectRequest = {
      trangThai: orderStatus, //3 trang thai open, confirmed, cancel
    };
    const orderUpdate = {
      method: "PUT",
      body: JSON.stringify(vObjectRequest),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };

    fetchApi(
      `http://42.115.221.44:8080/devcamp-pizza365/orders/${props.userOrder.id}`,
      orderUpdate
    )
      .then((data) => {
        console.log(data);
        props.setOpenModalUpdateUser(false);
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setTimeout(() => {
          handleShowSnackbar();
        }, 1000);
      });
  };

  return (
    <>
      <Modal
        open={props.openModalUpdateUser}
        onClose={handleCloseModalUpdateUser}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={styleBox}>
          <Grid container>
            <Grid item xs={12} md={12} sm={12} lg={12}>
              <Typography
                sx={{ py: "0.5rem" }}
                variant="h4"
                component="h4"
                align="left"
                fontWeight="bold"
              >
                User Info
              </Typography>
            </Grid>
            <Grid container xs={12} md={12} sm={12} lg={12}>
              <Grid item xs={6} md={6} sm={6} lg={6}>
                <TextField
                  value={props.userOrder.hoTen}
                  required
                  label="Họ Tên"
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6}>
                <TextField
                  value={props.userOrder.soDienThoai}
                  required
                  label="Số Điện Thoại"
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  value={props.userOrder.diaChi}
                  required
                  label="Địa Chỉ"
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  value={props.userOrder.email}
                  required
                  label="Email"
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  value={props.userOrder.loiNhan}
                  required
                  label="Lời Nhắn"
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <FormControl fullWidth error>
                  <InputLabel>Trạng Thái</InputLabel>
                  <Select
                    label="Trạng Thái"
                    size="medium"
                    sx={styleTextField}
                    defaultValue={props.userOrder.trangThai}
                    onChange={onChangeSelectStatus}
                  >
                    <MenuItem value="open">Open</MenuItem>
                    <MenuItem value="confirmed">Đã Xác Nhận</MenuItem>
                    <MenuItem value="cancel">Huỷ</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            {/* Pizza Size */}
            <Grid item xs={12} md={12} sm={12} lg={12}>
              <Typography
                sx={{ py: "0.5rem" }}
                variant="h4"
                component="h4"
                align="left"
                fontWeight="bold"
              >
                Pizza Order
              </Typography>
            </Grid>
            <Grid container xs={12} md={12} sm={12} lg={12}>
              <Grid item xs={6} md={6} sm={6} lg={6}>
                <FormControl fullWidth>
                  <InputLabel>Kích Cỡ</InputLabel>
                  <Select
                    label="Kích Cỡ"
                    size="medium"
                    sx={styleTextField}
                    value={props.userOrder.kichCo}
                    disabled
                  >
                    <MenuItem value="S">Size S</MenuItem>
                    <MenuItem value="M">Size M</MenuItem>
                    <MenuItem value="L">Size L</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6}>
                <TextField
                  label="Đường Kính"
                  value={props.userOrder.duongKinh}
                  required
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  label="Sườn"
                  value={`Sườn: ${props.userOrder.suon}`}
                  required
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  label="Salad"
                  value={props.userOrder.salad}
                  required
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  label="Số Lượng Nước"
                  value={props.userOrder.soLuongNuoc}
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                  required
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <TextField
                  label="Thành Tiền"
                  value={props.userOrder.thanhTien}
                  id="outlined-required"
                  size="medium"
                  sx={styleTextField}
                  variant="filled"
                  disabled
                  required
                />
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <FormControl fullWidth>
                  <InputLabel>Loại Pizza</InputLabel>
                  <Select
                    label="Loại Pizza"
                    size="medium"
                    sx={styleTextField}
                    value={loaiPizza}
                    disabled
                  >
                    <MenuItem value="seafood">Hải sản</MenuItem>
                    <MenuItem value="hawaii">Hawaii</MenuItem>
                    <MenuItem value="bacon">Thịt hun khói</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={6} md={6} sm={6} lg={6} pt={2}>
                <FormControl fullWidth>
                  <InputLabel>Loại Nước Uống</InputLabel>
                  <Select
                    label="Loại Nước Uống"
                    size="medium"
                    sx={styleTextField}
                    value={idLoaiNuocUong}
                    disabled
                  >
                    {props.drinkList.map((drink, index) => {
                      return (
                        <MenuItem value={drink.maNuocUong.toLowerCase()}>
                          {drink.tenNuocUong}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} md={12} sm={12} lg={12} pt={2}>
                <TextField
                  id="outlined-required"
                  label="Mã Voucher"
                  size="medium"
                  style={{ width: "97%" }}
                  value={props.userOrder.idVourcher}
                  variant="filled"
                  disabled
                />
              </Grid>
              <Grid item xs={12} md={12} sm={12} lg={12} pt={2}>
                <Button
                  className="fw-bold"
                  variant="contained"
                  color="info"
                  size="small"
                  style={{ padding: "10px", float: "right" }}
                  // Gọi Hàm Xem Chi Tiết User Khi Click
                  // Input userObj
                  onClick={updateStatusOrder}
                >
                  Update Order
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      {/* Toast */}
      <Snackbar
        open={showSnackbar}
        autoHideDuration={3000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity="success"
          sx={{ width: "100%" }}
        >
          Thay Đổi Trạng Thái Thành Công
        </Alert>
      </Snackbar>
    </>
  );
}

export default ModalUpdateUser;
