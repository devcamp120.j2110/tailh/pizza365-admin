import {
  Button,
  Container,
  Grid,
  Pagination,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Box,
} from "@mui/material";
import { ImPlus } from "react-icons/im";

import ModalAddNewUser from "../Components/Modal/ModalAddNewUser/ModalAddNewUser";
import { useEffect, useState } from "react";
import ModalUpdateUser from "./Modal/ModalUpdateUser/ModalUpdateUser";
import ModalDeleteUser from "./Modal/ModalDeleteUser/ModalDeleteUser";
function DataTable() {
  // State
  const [userData, setUserData] = useState([]);
  const [userOrder, setUserOrder] = useState({});
  const [drinkList, setDrinkList] = useState([]);
  const [page, setPage] = useState(1);
  const [noPage, setNoPage] = useState(0);
  const limit = 10;
  const [openModalAddNewUser, setOpenModalAddNewUser] = useState(false);
  const [openModalUpdateUser, setOpenModalUpdateUser] = useState(false);
  const [openModalDeleteUser, setOpenModalDeleteUser] = useState(false);

  // Handle 
  // Hàm Lấy Dữ Liệu Order
  const getUserData = async () => {
    const response = await fetch(
      "http://42.115.221.44:8080/devcamp-pizza365/orders"
    );
    const data = await response.json();
    return data;
  };

  // Hàm Lấy Dữ Liệu Nước
  const getDrinkList = async () => {
    const response = await fetch(
      "http://42.115.221.44:8080/devcamp-pizza365/drinks"
    );
    const data = await response.json();
    return data;
  };

  // Chuyển Trang
  const changePageHandle = (event, value) => {
    setPage(value);
  };

  // Thêm Order
  const addNewUser = () => {
    // Hiển Thị Modal Insert
    setOpenModalAddNewUser(true);
  };

  // Update Order
  const updateUser = (userObj) => {
    setOpenModalUpdateUser(true);
    setUserOrder(userObj);
    console.log(userObj);
  };

  // Delete Order
  const deleteUser = (userObj) => {
    setOpenModalDeleteUser(true);
    setUserOrder(userObj);
    console.log(userObj);
  };

  useEffect(() => {
    getUserData()
      .then((data) => {
        console.log(data);
        setNoPage(Math.ceil(data.length / limit));
        setUserData(data.slice((page - 1) * limit, page * limit));
      })
      .catch((error) => {
        console.log(error);
      });
    getDrinkList()
      .then((data) => {
        console.log(data);
        setDrinkList(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [page, limit]);

  return (
    <Container maxWidth="lg" style={{ padding: "0" }}>
      <Typography
        sx={{ py: "0.5rem" }}
        variant="h2"
        component="h2"
        align="center"
        fontWeight="bold"
      >
        All Order Pizza
      </Typography>
      <Grid item xs={12} md={12} sm={12} lg={12} marginBottom={2}>
        <Button
          className="fw-bold"
          variant="contained"
          color="secondary"
          size="small"
          style={{ padding: "8px", fontSize: "16px" }}
          // Gọi Hàm Thêm User
          // Input userObj
          onClick={addNewUser}
        >
          <ImPlus className="fw-bold me-1"></ImPlus>
          Add Order
        </Button>
      </Grid>

      {/* Data Table */}
      <Grid item xs={12} md={12} sm={12} lg={12}>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="users table">
            <TableHead>
              <TableRow>
                <TableCell align="center">
                  <Box sx={{ fontWeight: "bold" }}>Order ID </Box>
                </TableCell>
                <TableCell align="center">
                  <Box sx={{ fontWeight: "bold" }}>Họ Tên </Box>
                </TableCell>
                <TableCell align="center">
                  <Box sx={{ fontWeight: "bold" }}>Kích Cỡ </Box>
                </TableCell>
                <TableCell align="center">
                  <Box sx={{ fontWeight: "bold" }}>Loại Pizza </Box>
                </TableCell>
                <TableCell align="center">
                  <Box sx={{ fontWeight: "bold" }}>Loại Nước Uống </Box>
                </TableCell>
                <TableCell align="center">
                  <Box sx={{ fontWeight: "bold" }}>Số Điện thoại </Box>
                </TableCell>
                <TableCell align="center">
                  <Box sx={{ fontWeight: "bold" }}>Địa Chỉ </Box>
                </TableCell>
                <TableCell align="center">
                  <Box sx={{ fontWeight: "bold" }}>Trạng Thái </Box>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {userData.map((row, index) => (
                <TableRow
                  key={index}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell align="center" component="th" scope="row">
                    {row.orderId}
                  </TableCell>
                  <TableCell align="center">{row.hoTen}</TableCell>
                  <TableCell align="center">{row.kichCo}</TableCell>
                  <TableCell align="center">{row.loaiPizza}</TableCell>
                  <TableCell align="center">{row.idLoaiNuocUong}</TableCell>
                  <TableCell align="center">{row.soDienThoai}</TableCell>
                  <TableCell align="center">{row.trangThai}</TableCell>
                  <TableCell align="center">
                    <Button
                      className="fw-bold"
                      variant="contained"
                      color="success"
                      size="small"
                      style={{ padding: "8px", marginRight: "8px" }}
                      // Gọi Hàm Update Trạng Thái User
                      // Input userObj
                      onClick={() => updateUser(row)}
                    >
                      Edit
                    </Button>
                    <Button
                      className="fw-bold"
                      variant="contained"
                      color="error"
                      size="small"
                      style={{ padding: "8px" }}
                      // Gọi Hàm Delete User
                      // Input userObj
                      onClick={() => deleteUser(row)}
                    >
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>

      {/* Phân Trang  */}
      <Grid
        item
        xs={12}
        md={12}
        sm={12}
        lg={12}
        marginTop={3}
        paddingBottom={2}
      >
        <Pagination
          onChange={changePageHandle}
          count={noPage}
          defaultPage={page}
        ></Pagination>
      </Grid>

      {/* Modal Add New */}
      <ModalAddNewUser
        openModalAddNewUser={openModalAddNewUser}
        setOpenModalAddNewUser={setOpenModalAddNewUser}
        drinkList={drinkList}
      ></ModalAddNewUser>

      {/* Modal Update User */}
      <ModalUpdateUser
        openModalUpdateUser={openModalUpdateUser}
        setOpenModalUpdateUser={setOpenModalUpdateUser}
        userOrder={userOrder}
        drinkList={drinkList}
      ></ModalUpdateUser>

      {/* Modal Delete User */}
      <ModalDeleteUser
        openModalDeleteUser={openModalDeleteUser}
        setOpenModalDeleteUser={setOpenModalDeleteUser}
        userOrder={userOrder}
      ></ModalDeleteUser>
    </Container>
  );
}

export default DataTable;
